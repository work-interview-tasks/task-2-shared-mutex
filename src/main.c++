#include <cassert>
#include <iostream>
#include <mutex>
#include <shared_mutex>
#include <thread>

class shared_mutex {
  std::mutex mutex;
  volatile mutable int counter = 0;

public:
  void lock() {
    while (counter > 0)
      ;
    mutex.lock();
  }
  void unlock() { mutex.unlock(); }
  void lock_shared() {
    mutex.lock();
    ++counter;
    mutex.unlock();
  }
  void unlock_shared() {
    assert(counter != 0);
    mutex.lock();
    --counter;
    mutex.unlock();
  }
};
template <typename T> class ThreadSafeCounter {
public:
  ThreadSafeCounter() = default;

  // Multiple threads/readers can read the counter's value at the same time.
  unsigned int get() const {
    mutex.lock_shared();
    unsigned int ret = value;
    mutex.unlock_shared();
    return ret;
  }

  // Only one thread/writer can increment/write the counter's value.
  void increment() {
    mutex.lock();
    value++;
    mutex.unlock();
  }

  // Only one thread/writer can reset/write the counter's value.
  void reset() {
    mutex.lock();
    value = 0;
    mutex.unlock();
  }

private:
  mutable T mutex;
  unsigned int value = 0;
};

int main() {
  ThreadSafeCounter<shared_mutex> counter;
  ThreadSafeCounter<std::shared_mutex> counter2;
  std::mutex mutex;

  auto increment_and_print = [&counter, &mutex]() {
    for (int i = 0; i < 3; i++) {
      counter.increment();
      {
        std::unique_lock lock(mutex);
        std::cout << std::this_thread::get_id() << ' '
                  << ("(" + std::to_string(counter.get()) + ")") << '\n';
      }
    }
  };

  std::thread thread1(increment_and_print);
  std::thread thread2(increment_and_print);

  thread1.join();
  thread2.join();

  std::cout << "Std shared mutex result" << std::endl;

  auto increment_and_print2 = [&counter2, &mutex]() {
    for (int i = 0; i < 3; i++) {
      counter2.increment();
      {
        std::unique_lock lock(mutex);
        std::cout << std::this_thread::get_id() << ' '
                  << ("(" + std::to_string(counter2.get()) + ")") << '\n';
      }
    }
  };

  std::thread thread3(increment_and_print2);
  std::thread thread4(increment_and_print2);

  thread3.join();
  thread4.join();
}
